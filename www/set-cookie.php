<?php

	//Set cookies - particularly needed for iPhones which don't support being set in Javascript directly using document.cookie particularly well.
	//See https://www.cookiestatus.com/safari/  and https://www.cookiestatus.com/
	
	

	//Old-style (prior to PHP7.3) header set directly:
	//$header = "Set-Cookie: " . $_REQUEST['name'] . "=" . $_REQUEST['value'] . ";" . $_REQUEST['params'];
	//header('Set-Cookie: cross-site-cookie=bar; SameSite=Strict;');		//E.g. 'Set-Cookie: mycookie=value; SameSite=Strict;'
	
	
	$arr_cookie_options = array (
                'expires' => time() + 60*60*24*365*3,		//Three years from now 
                'path' => '/', 
                'SameSite' => 'Strict' // None || Lax  || Strict
                );
              /*  
                'expires' => time() + 60*60*24*365*2,		//Two years from now 
                'path' => '/', 
                'domain' => '.example.com', // leading dot for compatibility or use subdomain
                'secure' => true,     // or false
                'httponly' => true,    // or false
                'samesite' => 'None' // None || Lax  || Strict
                */
    setcookie($_REQUEST['name'], $_REQUEST['value'], $arr_cookie_options); 
    
    echo $_REQUEST['name'] . " " . $_REQUEST['value'] . " " . $_REQUEST['params'];
?>
