<?php
	//Ensure no caching
	header("Cache-Control: no-store, no-cache, must-revalidate"); // HTTP/1.1
	header("Cache-Control: post-check=0, pre-check=0", false);
	header("Expires: Sat, 26 Jul 1997 05:00:00 GMT"); // Date in the past
	header("Pragma: no-cache"); // HTTP/1.0
	header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");

?><!DOCTYPE html>
<!--
    Licensed to the Apache Software Foundation (ASF) under one
    or more contributor license agreements.  See the NOTICE file
    distributed with this work for additional information
    regarding copyright ownership.  The ASF licenses this file
    to you under the Apache License, Version 2.0 (the
    "License"); you may not use this file except in compliance
    with the License.  You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing,
    software distributed under the License is distributed on an
    "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
     KIND, either express or implied.  See the License for the
    specific language governing permissions and limitations
    under the License.
-->
<html>
    <head>
        <meta charset="utf-8" />
        <meta name="format-detection" content="telephone=no" />
        <meta name="viewport" content="user-scalable=no, initial-scale=1, width=device-width, viewport-fit=cover" />
		<meta name="apple-mobile-web-app-capable" content="yes">
        <meta name="mobile-web-app-capable" content="yes">
		
	    
	    <link rel="apple-touch-icon" href="medimage-icons/apple-icon-152x152.png" />	
		<link rel="apple-touch-icon" sizes="128x128" href="icon.png">	
		<link rel="icon" type="image/x-icon" href="medimage-icons/favicon-16x16.png" />
		
		<!--<link rel="icon" href="favicon.ico">-->
		<link rel="apple-touch-icon" sizes="57x57" href="medimage-icons/apple-icon-57x57.png">
		<link rel="apple-touch-icon" sizes="60x60" href="medimage-icons/apple-icon-60x60.png">
		<link rel="apple-touch-icon" sizes="72x72" href="medimage-icons/apple-icon-72x72.png">
		<link rel="apple-touch-icon" sizes="76x76" href="medimage-icons/apple-icon-76x76.png">
		<link rel="apple-touch-icon" sizes="114x114" href="medimage-icons/apple-icon-114x114.png">
		<link rel="apple-touch-icon" sizes="120x120" href="medimage-icons/apple-icon-120x120.png">
		<link rel="apple-touch-icon" sizes="144x144" href="medimage-icons/apple-icon-144x144.png">
		<link rel="apple-touch-icon" sizes="152x152" href="medimage-icons/apple-icon-152x152.png">
		<link rel="apple-touch-icon" sizes="180x180" href="medimage-icons/apple-icon-180x180.png">
		<link rel="icon" type="image/png" sizes="192x192"  href="medimage-icons/android-icon-192x192.png">
		<link rel="icon" type="image/png" sizes="48x48" href="medimage-icons/windows-icon-48x48.png">
		<link rel="icon" type="image/png" sizes="32x32" href="medimage-icons/favicon-32x32.png">
		<link rel="icon" type="image/png" sizes="96x96" href="medimage-icons/favicon-96x96.png">
		<link rel="icon" type="image/png" sizes="16x16" href="medimage-icons/favicon-16x16.png">
		<link rel="icon" type="image/png" sizes="256x256" href="medimage-icons/windows-icon-256x256.png">
		<link rel="icon" type="image/png" sizes="24x24" href="medimage-icons/windows-icon-24x24.png">
		<link rel="manifest" href="medimage-icons/manifest.json">
		<meta name="msapplication-TileColor" content="#ffffff">
		<meta name="msapplication-TileImage" content="medimage-icons/ms-icon-144x144.png">
		<meta name="theme-color" content="#ffffff">
	    
	    
	    <meta http-equiv="Content-Security-Policy" content="default-src * https://medimage-pair.atomjump.com http://127.0.0.1:5566 http://localhost:5566;  script-src 'self' 'unsafe-inline'; style-src 'self' 'unsafe-inline'; media-src * blob; img-src 'self' data:; connect-src *;">
	     
		<link rel="stylesheet" href="css/onsenui.css"/>
		<link rel="stylesheet" href="css/onsen-css-components-purple-theme.css">
    	
    	<link rel="stylesheet" type="text/css" href="css/ionicons/css/ionicons.css">
    	<link rel="stylesheet" type="text/css" href="css/material-design-iconic-font/css/material-design-iconic-font.css">
    	<noscript>Sorry, 'JavaScript' is disabled in your browser. Please enabled JavaScript in your browser to be able to operate the MedImage app.</noscript>
    	
    	<script>
    		//Run these cookie checks before any other .js scripts are included, and may fail
    		//But keep the functionality up-to-date with index.js versions of the same functions
    	
    		
    		    function myTrim(x)
				{
					return x.replace(/^\s+|\s+$/gm,'');
				}

				function getCookie(cname)
				{	
					if(document && document.cookie) {
						var name = cname + "=";
						var ca = document.cookie.split(';');
						for(var i=0; i<ca.length; i++)
						{
							var c = myTrim(decodeURIComponent(ca[i]));// ie8 didn't support .trim();
							if (c.indexOf(name)==0) return c.substring(name.length,c.length);
						}
					}
					return "";
				}


				function cookieOffset()
				{
				  //Should output: Thu,31-Dec-2020 00:00:00 GMT
				  var cdate = new Date;
				  var expirydate=new Date();
				  expirydate.setTime(expirydate.getTime()+(365*3*60*60*24*1000));	//3 years, although 2 years may be the limit on some browsers
				  var write = expirydate.toGMTString();
				  
				  return write;
				}
								
    		    
				function localStorageGetItem(mykey) {
				
					return getCookie(mykey);	
				}
	
	
    
				function localStorageSetItem(mykey, value) {
										
					//Now set the cookie locally
					document.cookie = mykey + '=' + encodeURIComponent(value) + '; path=/; SameSite=Strict; expires=' + cookieOffset() + ';';		//Note: strict means cookies only used by this site
										
					if(mykey !== "currentPhotoId") {		//We don't want to call the server every time we take a photo. It doesn't matter if
														//this gets reset on an iPhone, as it is just a current count. The 7-day cookies should be fine.
						var data = {};	
						data.name = mykey;
						data.value = value;
					
						
						//And start a server-side setting of the cookie via an http response. Important for iPhones.
						if(window.jQuery) {			//Note: the first time we run this, to do the checking of whether the cookies are working,
															//jQuery won't have been defined. We can skip the secondary setting of the cookies in this case, anyway.
							
							jQuery.ajax({
									url: "/cook/set-cookie.php",
									data: data,
									method:"POST",
									cache:false,
									crossDomain: false,
									global: false,
									error: function(err) {
										console.error(err);
									},
									success: function(data) {
										console.log(data);
									},
									complete:function(){
										console.log("Request finished.");
									}
							});
						}
					}
				}
				
				function localStorageRemoveItem(mykey) {
    	 			document.cookie = mykey + '=; SameSite=Strict; expires=Thu, 01 Jan 1970 00:00:00 GMT';  
				}
    
				function localStorageClear() {
					//Note: this won't remove different paths
					var c = document.cookie.split("; ");
			 		for (i in c) {
			  			document.cookie =/^[^=]+/.exec(c[i])[0]+"=; SameSite=Strict; expires=Thu, 01 Jan 1970 00:00:00 GMT"; 
			  		}  

				}
    	
    	
    		
    		   function reportStorage(storageWorking) {
    		   		
    		   		if(storageWorking === false) {
    		   	
						alert("Warning: Your browser needs '1st party cookies' to be enabled to be able to remember the MedImage Server connections between visits. How to do this differs per browser, but look under your browser's Privacy Settings to correct this. You may also try a different browser.");  
					}
    		   }
    	
			   function checkLocalStorageEnabled() {
					//Checks if local storage, currently cookie-based, existings and is working
					
					try {
				 		localStorageSetItem("ce", "exists");
				 		//Wait for a fraction of a second
				 						 		
				 		setTimeout(function() {
				 			try {
						 		var item = localStorageGetItem("ce");
								
								if(!item) {
									reportStorage(false);
									return false;
								} else {
									if(item === "exists") {
										reportStorage(true);
										return true;
									} else {
										//Stored connections not being held correctly
										reportStorage(false);
										return false;
									}
								}
							} catch(err) {
								reportStorage(false);
								return false;
							}
						}, 10);
					} catch(err) {
						reportStorage(false);
						return false;
					}
				
				}
				
				
				//Check if local storage exists, and provide a warning if not
				setTimeout(function() {
					checkLocalStorageEnabled();
				}, 50);
				
			
    	</script>
      	<script src="js/onsenui.js"></script>
    	<script src="js/jquery-3.4.0.js"></script>
  	
    	
    	
    	
		<script>
		
	      // This event fires when the keyboard will hide
			window.addEventListener('native.keyboardshow', keyboardShowHandler);



			function keyboardShowHandler(e){
				//$("body").addClass("keyboardOn");
				var deviceHeight = window.innerHeight;
				var keyboardHeight = e.keyboardHeight;
				var deviceHeightAdjusted = deviceHeight - keyboardHeight;//device height adjusted
				deviceHeightAdjusted = deviceHeightAdjusted < 0 ? (deviceHeightAdjusted * -1) : deviceHeightAdjusted;//only positive number
				document.getElementById('page').style.height = deviceHeightAdjusted + 'px';//set page height
				document.getElementById('page').setAttribute('keyBoardHeight', keyboardHeight);//save keyboard height
			}

			// This event fires when the keyboard will show
			window.addEventListener('native.keyboardhide', keyboardHideHandler);

			function keyboardHideHandler(e){
				   setTimeout(function () {
						document.getElementById('page').style.height = 100 + '%';//device  100% height
					}, 100);
			}
			
			
			function checkPlatform() {
				var platform = "Desktop";	//Assume this to begin with
				
				var isiOSLike = navigator.platform.match(/(iPhone|iPod|iPad)/i) ? true:false;				
				if(isiOSLike) platform = "iOS";
				if(navigator.platform === 'MacIntel' && typeof navigator.standalone !== "undefined") platform = "iOS";	//See https://stackoverflow.com/questions/57765958/how-to-detect-ipad-and-ipad-os-version-in-ios-13-and-up
				
				var userAgent = navigator.userAgent.toLowerCase();
				var android = userAgent.indexOf("android") > -1;
				if(android) platform = "Android";
						
				return platform;

			}
			
			
			var platformGuideIndex = {
				"iOS": "ios-guide",
				"Android": "android-guide",
				"Desktop": "desktop-guide"           
			};
			
			var platformButtonIndex = {
				"iOS": "ios-button",
				"Android": "android-button",
				"Desktop": "desktop-button"           
			};
			
			function selectGuide(detectedPlatform) {
			
				//Show the relevant text block
				jQuery("#" + platformGuideIndex[detectedPlatform]).toggle();
				if(detectedPlatform != "iOS") {
					$('#ios-guide').hide(); 
				}
				if(detectedPlatform != "Android") {
					$('#android-guide').hide(); 
				}
				if(detectedPlatform != "Desktop") {
					$('#desktop-guide').hide();
				}
			
				if(jQuery("#" + platformButtonIndex[detectedPlatform]).hasClass("highlighted")) {
					jQuery("#" + platformButtonIndex[detectedPlatform]).removeClass("highlighted");
				} else {
					jQuery("#" + platformButtonIndex[detectedPlatform]).addClass("highlighted");
				}
				
				if(detectedPlatform != "iOS") {
					$('#ios-button').removeClass("highlighted");
				}
				if(detectedPlatform != "Android") {
					$('#android-button').removeClass("highlighted"); 
				}
				if(detectedPlatform != "Desktop") {
					$('#desktop-button').removeClass("highlighted");
				}
				
				
				
			}
			
			function showDefaultPlatform() {
				//Decide on active class based on platform used:
				$('#ios-guide').hide(); 
				$('#android-guide').hide(); 
				$('#desktop-guide').hide();
				$('#ios-button').removeClass("highlighted");
				$('#android-button').removeClass("highlighted"); 
				$('#desktop-button').removeClass("highlighted");
				
				if($('#add-to-home').is(":hidden")) {
					$('#add-to-home').show(); 
					$('#install-box-background').show();
					var detectedPlatform = checkPlatform();
			
					selectGuide(detectedPlatform);
				} else {
					//Leave everything unhighlighted
					$('#add-to-home').hide(); 
					$('#install-box-background').hide();
				}
				
				
				
				return false;
			}
			
			function introBeta() {
				if(confirm("'ID scanning' is one of the features in the new MedImage Beta App. Would you like to try the Beta out now?  (Note: you can use both apps at the same time, but you will need a fresh 4-digit pairing code for the Beta)")) {
					//window.location.href = "https://medimage-app-beta.atomjump.com/";
					window.open("https://medimage-app-beta.atomjump.com/","_blank", "width=900, height=700");	//,"_blank","toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=yes, resizable=no, copyhistory=yes, width=900, height=700"
				}
				return false;
			}
			
		</script>

        <link rel="stylesheet" type="text/css" href="css/index.css" />
        <title>MedImage</title>
    </head>
    <body>
    	<style><item name="android:windowTranslucentStatus">true</item></style> <!-- Ensure clicking inside box scrolls down -->
    	<ons-navigator>
         <ons-page>
	        
		    <ons-fab style="z-index: 800;" position="bottom right" onclick="app.openSettings();">
		      <ons-icon icon="md-menu" ></ons-icon>
		    </ons-fab>
		    
		    <ons-fab style="z-index: 800;" position="bottom left" onclick="showDefaultPlatform();">
		      <ons-icon icon="md-bookmark" ></ons-icon>
		    </ons-fab>
		    
		
	
 
        <p style="text-align:center;">
        <!--<span class="big-text" onclick="navigator.notification.alert('Enter a patient or image ID, which names the image folder. The second word names the image file. The [ID] button allows you to snap a photo and detect an ID in English.', function(){}, 'Optional');">Image ID <small >(e.g. NHI) <ons-icon icon="ion-ios-help" size="20px" fixed-width="true"></small><br/></span><input id="id-entered" type="text" size="9" class="resizedTextbox" value=""><span id="waiting-icon" style="display: none;"><img src="img/ajax-loader.gif" width="28" height="28" style="position: relative; top: 6px; margin-left: 12px;"></span><span class="fakefile-scan"><input class="file" type="file" id="scan-button" accept="image/jpeg" capture="camera" onclick="return introBeta();"></span>-->
        
        <!-- GOOD:  <span class="big-text" onclick="navigator.notification.alert('Enter a patient or image ID, which names the image folder. The second word names the image file.', function(){}, 'Optional');">Image ID <small >(e.g. NHI) <ons-icon icon="ion-ios-help" size="20px" fixed-width="true"></small><br/></span><input id="id-entered" type="text" size="9" class="resizedTextbox" value=""> -->
        
             <span class="big-text" onclick="navigator.notification.alert('Enter a patient or image ID, which names the image folder. The second word names the image file.', function(){}, 'Optional');">Image ID <small >(e.g. NHI) <ons-icon icon="ion-ios-help" size="20px" fixed-width="true"></small><br/></span><input id="id-entered" type="text" size="9" class="resizedTextbox" value=""><span class="fakefile-scan" onclick="return introBeta();"></span>
        </p>

 		<div style="text-align:center;">
            <span id="currentPC"  class="pc-text"></span>
        </div>
        <div class="fakefile">
       		<input class="file" type="file" id="mypic" accept="image/jpeg" capture="camera">
       		<!-- For future improvement: you can add accept: accept="image/jpeg, application/pdf" on an option. This allows pdf uploads (though it
       		currently calls it xyz.jpg.pdf) However, each photo gives you a choice of using the camera or the file manager, so it is a slower interface)
       		Worth putting on an option though!
       		-->
       </div>
       
 
        <div style="text-align:center;">
            <span id="notify" class="notify-text" style="padding-left:4px; padding-right:4px;"></span>
            &nbsp;<span id="status" class="notify-text"></span>
            <br/><br/><span id="cancel-trans" class="cancel-text"></span>
            <br/><span id="general-warning" class="cancel-text"></span>
        </div>
        
        <!--Was in here - needed still?: <br/>-->
        
        
       <div id="install-box-background" style="display: none;">
        	<div id="install-box" >
        
        		
				<div id="add-to-home" style="display:none;">
					<small>Add app to your Homescreen <span style="float: right;"><a href="javascript:" onclick="return showDefaultPlatform();" ><ons-icon size="30px" style="color: black;" icon="md-close"></ons-icon></a></span><br/><a id="ios-button" class='button' style='margin:4px;' onclick="return selectGuide('iOS');" href='javascript:'>iOS</a>&nbsp;<a id="android-button" class='button' style='margin:4px;' onclick="return selectGuide('Android');" href='javascript:'>Android</a>&nbsp;<a id="desktop-button" class='button' style='margin:4px;' onclick="return selectGuide('Desktop');" href='javascript:'>Desktop</a></small><br/><br/>
				</div>
				
				<div id="ios-guide" style="display:none;">Tap <img src="img/safari-add.png" width="25" height="30">, usually along the bottom of the screen, then swipe up, and tap 'Add to Home Screen'. <a href="#ios-more-info" onclick="$('#ios-more-info').toggle();">More Info</a><br/><br/>
					<div id="ios-more-info" style="display:none;">
						<small><strong>Troubleshooting:</strong> if you don't see the 'Add to Home Screen' option, scroll to the bottom and tap 'Edit Actions', then tap 'Add' next to the 'Add to Home Screen'. After that, try again.</small> Or see <a href="https://support.apple.com/en-nz/guide/iphone/iph42ab2f3a7/ios#iph4f9a47bbc" target="_blank">Apple's Guide</a><br/><br/>								
						
					</div>
				</div>
				
				<div id="android-guide" style="display:none;">Tap the <img src="img/chrome-menu.png"width="20" height="20"> in the top right corner of the screen, and then choose 'Add to Home Screen'. <a href="#android-more-info" onclick="$('#android-more-info').toggle();">More Info</a><br/><br/>
					<div id="android-more-info" style="display:none;">
						<small><strong>Troubleshooting:</strong> If you cannot see the 3-dot menu, you may be in another app's internal browser. Run the browser app directly (e.g. Chrome or Firefox Fennec), and then open the MedImage app address from there.</small> Or see <a href="https://www.wikihow.com/Set-a-Bookmark-Shortcut-in-Your-Home-Screen-on-Android" target="_blank">More Tips</a><br/><br/>								
						
					</div>
				</div>
				
				<div id="desktop-guide" style="display:none;">Click the <img src="img/firefox-drag-desktop.png"> or <img src="img/chrome-desktop-drag.png"> or <img src="img/lock.png"> style icon in the address bar, to the left of the address, and depending on the browser, drag this onto your desktop. <a href="https://www.hellotech.com/guide/for/how-to-create-a-desktop-shortcut-to-a-website">More Details</a><br/><br/>In Edge on Windows, use <img src="img/edge-menu.png" width="30" height="20"> > "Apps" > "Install this site as an app".<br/>
				</div>

        	</div>
        </div>
		
        
        <div id="settings-popup" style="display:none;">
        	<div id="inner-popup">
        		
        		<span class="big-text">Settings</span><br/><br/>
        		
        		<ons-fab position="top right" onclick="app.closeSettings(); ">
		      		<ons-icon icon="md-close" ></ons-icon>
		    	</ons-fab>
        		
        		
        		<div id="settings"></div>
        		<p>
        			<ons-button style="background-color: #7c5599; color: white;" modifier="large" onclick="app.newServer();">Add a New PC</ons-button>
	        	</p>
	        	
	        	 <ons-list>
					<ons-list-item>
					  <div class="center">
						ID writes a folder
					  </div>
					  <div class="right">
						<ons-switch style="color: #7c5599;" id="always-create-folder" checked></ons-switch>
					  </div>
					</ons-list-item>
					<ons-list-item>
						<div class="right">
								<a class="small-text" target="_blank" href="http://medimage.co.nz/guide/#professional">Version: 2.5.0<a/>
						</div>
					</ons-list-item>
				  </ons-list>
	        	
	        	
				<p>
					<a class="small-text" href="javascript" onclick="return app.factoryReset();">Reset Factory Settings</a>
					
				</p>
				<p>
					<a style="color: #888;" class="small-text" href="javascript:" onclick="$('#show-my-data-options').toggle();">My Data <ons-icon icon='md-cloud-download'></ons-icon></a>
				</p>
				<div id="show-my-data-options" style="display: none;">
					<p>
						<a style="color: white; background-color: #7c5599;" class="small-text button" href="javascript:" onclick="$('#import-my-data').hide(); return app.showMyData();">View <ons-icon icon='md-cloud-download'></ons-icon></a>  <a style="color: white; background-color: #7c5599;" class="small-text button" href="javascript:" onclick="$('#import-my-data').show(); $('#show-my-data').hide();">Import <ons-icon icon='md-cloud-upload'></ons-icon></a>
					</p>
					
					
					<p id="import-my-data" style="text-align:center; display: none;">
						<span class="small-text">Please paste your "Raw Data" below</span><br/><textarea style="padding:5px;" id="import-raw-data" rows="4" cols="40"></textarea><br/><span class="small-text">Export this data from 'Settings' > 'My Data' > 'View'</span>
					
						<br/><br/>
						<ons-button style="background-color: #7c5599; color: white;" modifier="large--cta" onclick="app.importCookies(); return false;"><ons-icon icon='md-cloud-upload'></ons-icon>&nbsp;&nbsp;Import Personal Data</ons-button>
					</p>
					
					<p id="show-my-data" style="display: none; background-color: rgb(238, 238, 238); opacity: 1.0; padding: 10px; padding-bottom: 100px; width:100%;">
						<span class="small-text" style="color: #555;">Your MedImage app data is stored in your local browser's 'cookies' for up to 2 years, and is annually refreshed if the app is being used. You can copy and paste this central app data into a local file, which can then be imported with the 'Import' option above, to either refresh this device, or create a new secondary device.
						<br/><br/>Please store any data here in a <b>secure location</b>.<br/></span><br/>
						<b>Raw Data:</b><br/><textarea id="raw-data" rows="4" cols="40"></textarea><br/><br/>
						<b>Readable Version:</b><br/><textarea id="human-data" rows="4" cols="40"></textarea><br/><br/>
						<b>Images Stored:</b><br/><span id="stored-image-description" class="small-text" style="color: #555;"></span><br/><img id="stored-image" src="" width="90%">
					</p>
				</div>
			</div>
        </div>
        
        <!-- See https://ourcodeworld.com/articles/read/322/how-to-convert-a-base64-image-into-a-image-file-and-upload-it-with-an-asynchronous-form-using-jquery -->
        
        <img style="display: none;" id="myImage">
		
		<form id="photo-sending-frm" method="post" action="/replaced-dynamically-with-medimage-server-path/">
		</form>

        <style>
        input.file {
			position: relative;
			text-align: right;
			-moz-opacity:0 ;
			filter:alpha(opacity: 0);
			opacity: 0;
			z-index: 1002;
			width: 300px; 
			height: 300px;
			
			margin-left: auto; 
			margin-right: auto;
			margin-bottom:10px;
			margin-top:6px;
		}
		

		.fakefile {
			position: relative;
			z-index: 1000;
			width: 300px; 
			height: 300px;
			background: url('img/logo@2x.png') no-repeat center center fixed;
			-webkit-background-size: cover;
      		-moz-background-size: cover;
      		-o-background-size: cover;
      		background-size: cover;
      		background-attachment:scroll;
  			
  			margin-left: auto; 
			margin-right: auto;
			margin-bottom:10px;
			margin-top:6px;
		}
		
		.fakefile-scan {
			position: relative;
			z-index: 1000;
			top: 10px;
			margin-left: 10px;
			width: 40px; 
			height: 40px;
			background: url('img/id-link.png') no-repeat center center fixed;
			-webkit-background-size: cover;
	      		-moz-background-size: cover;
	      		-o-background-size: cover;
	      		background-size: cover;
	      		background-attachment:scroll;
	      		display: inline-block;	
		}
		

        </style>
        
        <!-- Cache these icons now for later dynamic use. These are hidden via their opacity -->
        <ons-icon style="vertical-align: middle; color:#f7afbb; filter:alpha(opacity: 0); opacity: 0; -moz-opacity:0;" size="30px" icon="fa-close"></ons-icon>
        <ons-icon style="vertical-align: middle; color:#DDD; filter:alpha(opacity: 0); opacity: 0; -moz-opacity:0;" size="20px" spin icon="fa-spinner"></ons-icon>
        
       <canvas style="display: none;"></canvas>
       <script>
 
       
		  var input = document.querySelector('input[type=file]'); 
		  input.onchange = function () {
			var file = input.files[0];
			drawOnCanvas(file);   
		  };
		 
		 
		  function drawOnCanvas(file) {
			var reader = new FileReader();
			reader.onload = function (e) {
			  var dataURL = e.target.result,			 
			  c = document.querySelector('canvas'), 
			  ctx = c.getContext('2d'),
			  img = new Image();
			  
		 
			  img.onload = function() {
				c.width = img.width;
				c.height = img.height;
				ctx.drawImage(img, 0, 0);
			  };
		 	
			  //This line is not necessary - but would be used if you are drawing the image: img.src = dataURL;
			  
			  
			  //Click the big button and pass in the image data as jpeg
              app.bigButton(dataURL);
			  
			};
		 
			reader.readAsDataURL(file);
			
		  }
		 
		 

       </script>
       
       

        
        
        <script type="text/javascript" src="cordova.js"></script>
        <script type="text/javascript" src="js/pub/index-2-4-5.js"></script> <!-- Update number for new version to prevent caching -->
        <script type="text/javascript">
          ons.ready(function() {
	        	
	        
	        	
	        
	        	//Global databases
			  	var idbSupported = false;		//Whether we can store photos in an indexeddb database within this browser, or not
			  	app.idbSupported = false;
			  	var sendCacheRam = [];			//If we cannot use an indexeddb database within this browser, store photos temporarily in RAM memory
			  	app.medImageSendCacheDb = {};
			 
			 
			  	//document.addEventListener("DOMContentLoaded", function(){
			  	//Set up the indexedDb database
				if("indexedDB" in window) {
					idbSupported = true;
					app.idbSupported = true;
				} 
				
				if(idbSupported == true) {
					//Seem to have support for indexDB
					app.medImageSendCacheDb = indexedDB.open("sendCache");
					
				} else {
					alert("Sorry, additional storage is not supported, and unsent photos will not be remembered when you revisit this app.");
					app.medImageSendCacheDb = {};
					
				}

				app.medImageSendCacheDb.onupgradeneeded = function() {
				  // The database did not previously exist, so create object stores and indexes.
				  app.medImageSendCacheDb = app.medImageSendCacheDb.result;
				  var store = app.medImageSendCacheDb.createObjectStore("images", {keyPath: "imageId"});
			  
				};

				
				app.medImageSendCacheDb.onsuccess = function() {
				  	app.medImageSendCacheDb = this.result;
				  	

				  
				  	// Init code here
        			app.initialize();
				};
		
		
				//End of db setup
		
	        
	    });
	     
	     
	   
	     
	     
	     
        var mySwitch = document.getElementById("always-create-folder");
        
        if(checkPlatform() === "iOS") {
        	//On iOS we are going to register the change when you click the Close button, or some other button.
		} else {
			//Every other browser platform supports this correctly
			mySwitch.addEventListener('change', function() {	
				app.saveIdInput(document.getElementById("always-create-folder").checked);
			});
			
		}
		
		
          
		</script>
		
		
		
          
          
     </ons-page>
     
     
    </ons-navigator>  
    </body>
</html>
