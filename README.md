<img src="http://medimage.co.nz/wp-content/uploads/2018/04/icon-60.png">

# MedImage App

This app allows a medical doctor to take a photo with their iPhone or Android phone, and transfer the image
securely to their desktop system, which has the MedImage Server installed on it.

# Requirements


The project was originally built with the PhoneGap Build tool and released as Android and iPhone app-store apps.

However, since Phonegap has been removed, a migration has occurred to Apache Cordova, which is fundamentally the same system. 

We have also now migrated away from app-store apps, and our browser app is the primary version.

The latest migration has been away from Cordova itself, to a pure website edition that runs within Apache2.  This provides the maximum level of security.


The desktop system should have the MedImage Server at https://github.com/atomjump/medimageserv installed first.


# Branches and Status


## Apache Browser version

The 'apache' branch is now considered the main version of the software. 

To install, first install Apache2 and any version of PHP. On Windows, this is most easily done with a WAMP installation binary e.g. https://www.wampserver.com/en/ (Note: MySQL is not used)

Install 'git', and then in a command box type:
```
cd /var/www/html    [or some equivalent folder on e.g. a Windows install]
git clone https://git.atomjump.com/medimage.git my-medimage-app
```

On some systems you may need to add 'sudo' before these commands.

You should install the app as an SSL version, and to do this you will need some keys. Add a new virtual server to the Apache sites-enabled configuration file e.g. /etc/apache2/sites-enabled/default-ssl.conf. For example:
```
       <VirtualHost *:443>
                ServerName medimage-app.yoursite.com
                #REPLACE yoursite.com with your domain above
 
                SSLEngine on
                SSLCertificateFile    /etc/apache2/ssl/ssl.crt
                SSLCertificateKeyFile 	/etc/apache2/ssl/private.key
                SSLCertificateChainFile 	/etc/apache2/ssl/sub.class1.server.ca.pem

                SSLProtocol all -SSLv2 -SSLv3
                SSLHonorCipherOrder on
                SSLCipherSuite "EECDH+ECDSA+AESGCM"	
                #INCLUDE your own SSL options above	


                BrowserMatch "MSIE [2-6]" \
                                nokeepalive ssl-unclean-shutdown \
                                downgrade-1.0 force-response-1.0
                # MSIE 7 and newer should be able to use keepalive
                BrowserMatch "MSIE [17-9]" ssl-unclean-shutdown

                DocumentRoot /var/www/html/my-medimage-app/www

                <Directory "/var/www/html/my-medimage-app/www">
                        AllowOverride All
                        Require all granted
                </Directory>
        </VirtualHost>  
```

Where the "DocumentRoot" and "<Directory "..."> should point to the www folder within your my-medimage-app folder.

and restart Apache2

```
service apache2 restart
```

Then you should be able to run https://medimage-app.yoursite.com in a browser and it will open the app. Double check in a browser Developer pane that all files are being loaded OK.

Note: You may want, or need, to open your firewall to port 443.


__Notes:__ This branch is based on the 'browser' branch, which is built with Cordova, but instead operates from within Apache.  The main additions and differences between
the 'apache' branch and the 'browser' branch are:

* the dynamically requested Cordova plugin js files, which are special-cased when running within Cordova itself, have been copied and pasted out from within a browser running the 'browser' branch version. (this is currently a manual process that we have done within Firefox > Web developer > Network pane. There may be another, more efficient way to get this publishable info out.)  If you need to add, change, or update these libraries, you will need to run the Browser branch, first, and repeat the manual copy process.
* /icons/ folder is renamed to be /medimage-icons/. Apache treats the icons folder as a special directory.
* .htaccess has been added and provides Apache-based security. It restricts certain file-types.



## Browser branch

This is a version of the app ('browser' and 'browser-test' branches) which you can use if you need more Cordova libraries during development.  Note: the app-store versions have been depreciated.

Required: You will need "npm" and "nodejs" to be installed, which should be there after a MedImage Server Windows installation

Install in a command box with:
```
git clone https://git.atomjump.com/medimage.git
cd medimage
npm install -g cordova  
cordova platform add browser
cordova build browser
cordova run browser
```

Then you can access your app from within a browser on port 8000 e.g.

```
http://localhost:8000
```

Optional: You may want to open your firewall to port 8000, to allow access over the LAN.

Optional: You may want to use an Apache Proxy, or some equivalent, to get port 8000 running from a :443 SSL domain


__Important for iPhone support in the Browser branch__

To ensure iPhone users retain their data in Safari, you need to implement an alternative server script that sets the cookies via
an http header rather than with the basic "document.cookie = ''" used in the NodeJS server's HTML page. These server-side cookies need 
to be set due to the iOS 1st party cookie requirements, see: https://www.cookiestatus.com/safari/. Without this script, iOS phones 
will likely not retain their stored pairings past 7 days of inactivity on the app.

For example, we use an Apache Proxy to get port 8000 running from a :443 SSL domain, but let through requests in the /cook folder 
to go to a PHP script there for server-side cookie responses. 


For this setup, you will need PHP >= 7.3.

```
       <VirtualHost *:443>
                ServerName medimage-app.yourhost.com
                ProxyPreserveHost On

               ... [more in here] ...
               
                DocumentRoot /var/www/html/medimage-app/www


                # setup the proxy
                SSLProxyEngine on
                <Proxy *>
                        Order allow,deny
                        Allow from all
                </Proxy>
                ProxyPass /cook !
                ProxyPass / http://localhost:8000/
                ProxyPassReverse /cook !
                ProxyPassReverse / http://localhost:8000/
        </VirtualHost>  

```

The line "ProxyPass /cook !" prevents /cook/set-cookie.php from going to the NodeJS server, and instead it passes it back to the PHP/Apache2 server.


### Updates

To upgrade to the latest version, you can follow steps similar to these below:
```
cd path/to/app-server
git pull
[if there have been any changes in the .htaccess files, you may also need to run 'service apache restart']
```

On some systems you may need to add 'sudo' before these commands.

__'browser' version notes__:
```
cd path/to/app-server
git pull
[kill the cordova process at the operating system level, e.g. by pressing 'ctrl-c' while the window has the focus, or using some other process kill method]
cordova run browser
```
Your app server should be accessible again within 10 seconds or so.


## App-store apps

These have now been depreciated.

The 'android' branch is for Android phones, while the 'master-ios' branch is for iPhones. There are subtle differences (e.g. com.phonegap.medimage for iOS vs com.atomjump.medimage for Android), but care should be taken to keep shared code changes reflected across, as much as possible, between the two code-bases.
On iOS, you will need distribution keys.

Current status:

* Builds successfully on Android. Sends across a blank 0 byte image. It does not register as being connected to Wifi. Camera incorrectly starts with front-facing camera each time.
* Build failures on iOS. Seems to be problems with versions of the cordova-plugin-file-transfer library and cordova-plugin-dialogs at this stage.
* Note for AtomJump internal developers: since we don't have the original Phonegap certificate, a completely new app will need to be released, and old users migrated to it, turning the old app into a 'Legacy' version. We have a placeholder on the Google appstore as "MedImage (Experimental)", which will eventually become "MedImage". 



## Development work

* On the 'browser' version, test changes on the 'browser-test' branch before merging into the 'browser' branch.
* On the 'apache' version, test changes on the 'apache-test' branch before merging into the 'apache' branch, and the 'master' branch, which is what the public see.


## Notes

* Switching over from filetransfer to HTML5
https://cordova.apache.org/blog/2017/10/18/from-filetransfer-to-xhr2.html

## Future work

* Package this up into a MedImage add-on as a .zip or .exe file.

# License

Application source code copyright (c) 2021 AtomJump Ltd. (New Zealand). All rights reserved.


Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.



